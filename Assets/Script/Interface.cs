﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour {

    public Transform dropdownMenu, prefabDados, dadosArea, gameArea;
    private List<Dropdown.OptionData> menuOptions;
    private Transform instancia;

    public float posX = 3f;
    public float posY = 3f;

    public Aleatorizador _aleatorizador;

    private void Start()
    {
        //Iniciar na opçao de dois dados
        dropdownMenu.GetComponent<Dropdown>().value = 1;
        JogarDados();
    }

    public void JogarDados() {
        int menuIndex = dropdownMenu.GetComponent<Dropdown>().value;
        //get all options available within this dropdown menu
        menuOptions = dropdownMenu.GetComponent<Dropdown>().options;
        string value = menuOptions[menuIndex].text;
        print(value);

        // limpar todos os dados na mesa
        foreach (Transform child in dadosArea)
        {
            GameObject.Destroy(child.gameObject);
        }

        // reseta as posicoes dos dados
        posX = 2f;
        posY = -2f;

        switch (value)
        {
            case "1 Dado":
                instancia = Instantiate(prefabDados, new Vector3(posX, posY, 15), Quaternion.identity);
                instancia.transform.SetParent(dadosArea.transform, false);
                _aleatorizador.numeroDados(1);
                break;

            case "2 Dados":
                
                for (int i = 0; i < 2; i++) {
                    instancia = Instantiate(prefabDados, new Vector3(posX, posY, 15), Quaternion.identity);
                    instancia.transform.SetParent(dadosArea.transform, false);
                    posX += 2f;
                }
                _aleatorizador.numeroDados(2);
                break;

            case "3 Dados":
                for (int i = 0; i < 3; i++)
                {
                    instancia = Instantiate(prefabDados, new Vector3(posX, posY, 15), Quaternion.identity);
                    instancia.transform.SetParent(dadosArea.transform, false);

                    // spawn do terceiro dado
                    if (posX == 4f)
                    {
                        posX = 2f;
                        posY = -4f;
                    }
                    else { 
                        posX += 2f;
                    }
                }
                _aleatorizador.numeroDados(3);
                break;

            case "4 Dados":
                for (int i = 0; i < 4; i++)
                {
                    instancia = Instantiate(prefabDados, new Vector3(posX, posY, 15), Quaternion.identity);
                    instancia.transform.SetParent(dadosArea.transform, false);
                    // spawn do terceiro dado
                    if (posX == 4f)
                    {
                        posX = 2f;
                        posY = -4f;
                    }
                    // spawn do quarto dado
                    else if (posX == 2f && posY == -4f)
                    {
                        posX = 4f;
                        posY = -4f;
                    }
                    else
                    {
                        posX += 2f;
                    }
                }
                _aleatorizador.numeroDados(4);
                break;
        }
    }
}
