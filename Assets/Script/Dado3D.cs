﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dado3D : MonoBehaviour
{
    public float thrustX, thrustY, thrustZ;
    public Rigidbody rb;

    private bool rotacao = true;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(thrustX, thrustY, thrustZ, ForceMode.Impulse);
        transform.Rotate(Random.Range(0f, 330f), Random.Range(0f, 330f), 0, Space.World);
    }

    private void Update()
    {
        if(rotacao)
        transform.Rotate(2, 1, 2, Space.World);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Chao") {
            rotacao = false;
        }
    }
    
}