﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovCelular : MonoBehaviour {

    private Aleatorizador _aleatorizadorScript;
    private Interface _interfaceScript;

    public Text aleatorizador;
    public Transform dadosArea;

    public Rigidbody2D rigidbody2D;

    // Use this for initialization
    void Start () {
        _aleatorizadorScript = GameObject.Find("Game_Controller").GetComponent<Aleatorizador>();
        _interfaceScript = GameObject.Find("Game_Controller").GetComponent<Interface>();
    }
	
	// Update is called once per frame
	void Update () {
        // mexer o celular para as direçoes em x
        if (Input.acceleration.x >= 0.7 || Input.acceleration.x <= -0.7) {
            aleatorizador.text = "" + Input.acceleration.x;
            _interfaceScript.JogarDados();
            //_aleatorizadorScript.MovCelular();
        }
        // mexer o celular para as direçoes em y
        if (Input.acceleration.x >= 0.7 || Input.acceleration.x <= -0.7)
        {
            aleatorizador.text = "" + Input.acceleration.x;
            _interfaceScript.JogarDados();
            //_aleatorizadorScript.MovCelular();
        }
        if (Input.GetMouseButtonDown(0))
            _interfaceScript.JogarDados();
    }

    void fisicaDado() {
        int count = 0;
        foreach (Transform child in dadosArea) {
            rigidbody2D = child.GetComponent<Rigidbody2D>();
            count++;
            switch (count) {
                case 1:
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.y);
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.x);
                    break;
                case 2:
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.y);
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.x);
                    break;
                case 3:
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.y);
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.x);
                    break;
                case 4:
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.y);
                    rigidbody2D.velocity = new Vector2(Random.Range(-20.0f, 20.0f), rigidbody2D.velocity.x);
                    break;
            }
        }


    }
}
