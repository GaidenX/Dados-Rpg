﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour {

	public string appID;
	private static bool initialized = false;

	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	public void Awake(){
		if (Advertisement.isSupported && initialized==false) {
			initialized = true;
			Advertisement.Initialize (appID,Debug.isDebugBuild);
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	public void ShowRewarded() {
		Advertisement.Show("rewardedVideoZone", new ShowOptions {
			resultCallback = result => {
				//acabou o rewarded ok
				//agora precisa dar o premio para o jogador
			}
		});
	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	public void ShowInterstitial() {
		Advertisement.Show("video", new ShowOptions {
			resultCallback = result => {

			}
		});
	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void StaticShowInterstital() {
		GameObject.FindObjectOfType<UnityAds>().ShowInterstitial();
	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	public static void StaticShowRewarded() {
		GameObject.FindObjectOfType<UnityAds>().ShowRewarded();
	}
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
}
